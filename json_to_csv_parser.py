"""!
@brief Streaming API Simple JSON Parser
@details A simple parser of the streaming API output json that
        will output the results in a csv file. Use it with an output file
        or print to standard output

@author Spiros Dimopoulos {sdim44@behavioralsignals.com}
@copyright 2019 Behavioral Signals Technologies
@todo Currently assumes that all response ids contain outputs at the same time-
    step. It will need adjustment if/when the results are provided at different
    time resolution.

"""
from __future__ import print_function
import json
import argparse
import sys

outputs = ['id','vocal_variety','intensity','strength','speaking_rate','engagement','positivity','politeness','emotion']
speakers = ['speaker1','speaker2']

def main():
    parser = argparse.ArgumentParser(
        description="Simple JSON to CSV Parser for Behavioral Signals \
        streaming output"
    )
    parser.add_argument(
        "-i",
        "--input",
        help="Input JSON",
        required=True
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Output CSV",
        default=None
    )
    args = parser.parse_args()

    try:
        inf = open(args.input, mode='r')
    except:
        print("Could not open input file. Exiting...", file=sys.stderr)
        return

    if args.output:
        try:
            outf = open(args.output, mode='w')
        except:
            print("Could not open output file. Exiting...", file=sys.stderr)
            return
    else:
        outf = None
    input_str=inf.read()
    inf.close()
    print("File opened. Converting single quotes to double and reading JSON", file=sys.stderr)
    input_str = input_str.replace("\'", "\"")
    try:
        frames=json.loads(input_str)
    except ValueError as e:
        print(e, file=sys.stderr)
        print("Could not read valid JSON data from input file. Exiting...", file=sys.stderr)
        return
    frames_list=[]
    row = []
    for output in outputs:
        if 'id' == output:
            row.append(output)
        else:
            for idx,speaker in enumerate(speakers):
                row.append(output+"_"+str(idx+1))
    if outf:
        outf.write(','.join(row)+'\n')
    else:
        print(','.join(row))
    for frame in frames:
        row = []
        for output in outputs:
            if output in frame and output == 'id':
                row.append(str(frame[output]))
            elif output in frame:
                for speaker in speakers:
                    if speaker in frame[output]:
                        try:
                            if 'value' in frame[output][speaker]:
                                row.append(str(frame[output][speaker]['value']))
                            else:
                                row.append(str(frame[output][speaker]))
                        except TypeError:
                            row.append(str(frame[output][speaker]))
                    else:
                        row.append("NA")
        if outf:
            outf.write(','.join(row)+'\n')
        else:
            print(','.join(row))

    if outf:
        outf.close()

    print ("Done", file=sys.stderr)

if __name__ == "__main__":
    main()
