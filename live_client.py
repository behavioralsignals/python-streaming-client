"""!
@brief Streaming API Example Client
@details This is a simple client of the streaming API. It is able to submit a
single audio file at a specified rate simulating a live recording streaming
session. Uses the ws4py and requests packages from pip.

@author Spiros Dimopoulos {sdim44@gmail.com}
@maintainer Spiros Georgiladakis {spgeo@behavioralsignals.com}
@copyright 2018 Behavioral Signals Technologies

"""

import json
import argparse
from live_api_client.streaming_client import StreamingClient


def process_live_audio(audio, channels, tasks=None, rate=1, servername='live.behavioralsignals.com',
                       cid=None, token=None, verbose=0, mode="external", dtt=0):
    loglevel = 30 - 10 * min(int(verbose), 2)

    ssl_suffix = "s"
    port = 80
    if mode == "internal":
        ssl_suffix = ""
        port = 8080

    protocol = f"http{ssl_suffix}"
    host = servername
    client_id = cid

    audio_data = {
        'path': audio,
        'name': audio,
        'calldirection': 1,
        'channels': channels
    }
    alerts_on = False
    if tasks:
        audio_data['tasks'] = tasks
        task_list = json.loads(tasks)
        for t in task_list:
            if "alerts" in t["name"] and t["options"]["activate"]:
                alerts_on = True

    streaming_client = StreamingClient()
    streaming_client.setup_connection(
        protocol=protocol,
        host=host,
        port=port,
        client_id=client_id,
        token=token,
        rate=rate,
        loglevel=loglevel,
        alerts_on=alerts_on,
        dtt=dtt
    )
    result, pid = streaming_client.run(audio_data=audio_data)
    return result, pid


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Simple CLI for Behavioral Signals streaming API"
    )
    parser.add_argument(
        "-s",
        "--servername",
        default="live.behavioralsignals.com",
        help="Streaming API Server Hostname",
    )
    parser.add_argument("-m", "--mode", default="external", help=argparse.SUPPRESS)
    parser.add_argument("-i", "--id", help="Client account ID", required=True)
    parser.add_argument("-t", "--token", help="Client Token", required=True)
    parser.add_argument(
        "-r",
        "--rate",
        default=1.0,
        type=float,
        help="How fast to send the audio compared to actual audio playtime (default 1x realtime)",
    )
    parser.add_argument(
        "-c", "--channels", default=2, type=int, help="Audio channels (1,2ch)"
    )
    parser.add_argument(
        "--tasks",
        help="Tasks request input (e.g., "
             "'[{\"name\":\"alerts\",\"options\":{\"activate\":true}},"
             "{\"name\":\"stream\",\"options\":{\"activate\":false}}]'"
    )
    parser.add_argument(
        "audio",
        help="Audio file to use for streaming. If the audio file extension given is wav, \
              the format is set to wav and a valid RIFF wave header is expected. If the \
              audio file extension is mp3, a valid mp3 header is expected. In all other \
              extensions the format is set to raw with the assumed audio properties are \
              8KHz, 16bit, unsigned integer"
    )
    parser.add_argument(
        "-o", "--output", help="File to store the result"
    )
    parser.add_argument('--verbose', '-v', type=int, default=1, help="Level of output logs")

    parser.add_argument('--dtt', type=int, default=0, help="Delay start transmission time after opening the websocket")

    args = parser.parse_args()

    result, pid = process_live_audio(audio=args.audio, channels=args.channels, tasks=args.tasks,
                                     rate=args.rate, servername=args.servername, cid=args.id,
                                     token=args.token, verbose=args.verbose, mode=args.mode, dtt=args.dtt)

    print(f"Result length: {len(str(result))}")
    if args.output:
        try:
            with open(args.output, 'w') as fout:
                fout.write(json.dumps(result))
        except IOError as err:
            print(err)
    else:
        print(result)
