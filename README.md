# python-streaming-client

Python client for streaming API

This is a simple client of the streaming API. It is able to submit a
single audio file at a specified rate simulating a live recording streaming
session. Uses the ws4py and requests packages from pip.

### How to Install

Open a virtual environment with python3 and install requirements by running `pip3 install -r requirements`

### How to Run

```
usage: live_client.py [-h] [-s SERVERNAME] -i ID -t TOKEN [-r RATE]
                      [-c CHANNELS] [--tasks TASKS] [-o OUTPUT] [--verbose]
                      audio

Simple CLI for Behavioral Signals streaming API

positional arguments:
  audio                 Audio file to use for streaming. If the audio file
                        extension given is wav, the format is set to wav and a
                        valid RIFF wave header is expected. If the audio file
                        extension is mp3, a valid mp3 header is expected. In
                        all other extensions the format is set to raw with the
                        assumed audio properties are 8KHz, 16bit, unsigned
                        integer

optional arguments:
  -h, --help            show this help message and exit
  -s SERVERNAME, --servername SERVERNAME
                        Streaming API Server Hostname
  -i ID, --id ID        Client account ID
  -t TOKEN, --token TOKEN
                        Client Token
  -r RATE, --rate RATE  How fast to send the audio compared to actual audio
                        playtime (default 1x realtime)
  -c CHANNELS, --channels CHANNELS
                        Audio channels (1,2ch)
  --tasks TASKS         Tasks request input (e.g., '[{"name":"alerts","options
                        ":{"activate":true}},{"name":"stream","options":{"acti
                        vate":false}}]'
  -o OUTPUT, --output OUTPUT
                        File to store the result
  --verbose, -v         Level of output logs
```

### Parse JSON to CSV

You can use the provided simple JSON to CSV parser so you can parse the streaming output into a csv. The columns will correspond to the different outputs and the rows to different response IDs.

1. Call the client to process a file and redirect output to a file
```
python3 ./live_client.py -s live.behavioralsignals.com -i cid -t 'token' -r 2 -c num_of_ch audio.wav -o audio.json
```
2. Use the parser to convert the JSON into a csv
```
python json_to_csv_parser.py -i audio.json -o audio.csv
```
3. Now you can view the results in a tool of you liking.
