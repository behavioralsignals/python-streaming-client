HTTP_200_OK = 200
HTTP_500_INTERNAL_SERVER_ERROR = 500


SERVICE_500_ERROR_RESPONSE = (
    {
        'code': HTTP_500_INTERNAL_SERVER_ERROR
    },

    HTTP_500_INTERNAL_SERVER_ERROR
)


DEFAULT_HANDSHAKE_PACKET = {
    'control': 'start',
    'pid': None,
    'format': 'raw',
    'channels': 2
}

KEEPALIVE_CONTROL_PACKET = {
    'control': 'keepalive',
    'pid': None
}


CLOSE_HANDSHAKE_PACKET = {
    'control': 'end'
}


WS_RESPONSE_TIME = 5

# Rate in which chunks are sent
STREAMING_RATE = 0.25

MAX_ACCEPTED_RESULTS_DELAY = 3

KEEPALIVE_TIME_PERIOD = 60
