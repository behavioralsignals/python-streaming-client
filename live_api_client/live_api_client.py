import os
import logging
import json
import threading
import websocket
import requests
from requests.exceptions import RequestException


from .globals import (
    HTTP_200_OK,
    SERVICE_500_ERROR_RESPONSE,
    DEFAULT_HANDSHAKE_PACKET,
    KEEPALIVE_CONTROL_PACKET,
    CLOSE_HANDSHAKE_PACKET
)


class LiveApiClient:
    """BSI Live API Client"""

    def __init__(self, host, port, client_id, token, results_handler, protocol='https', ws_event=None, loglevel=1):
        self.host = host
        self.port = port
        self.protocol = protocol
        self.client_id = client_id
        self.token = token
        self.is_final = False
        self.finish = False
        self.results_handler = results_handler

        self.ws_service_event = ws_event
        self.processing_state = None
        self.retry_time = 0
        self.failure = None
        self.identity = None

        self.configure_logger(loglevel)

        self.set_websocket_url()

        header = {'X-Auth-Token': token}

        self.websocket = websocket.WebSocketApp(
            self.ws_url,
            on_open=self.on_open,
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            header=header
        )

    def configure_logger(self, loglevel=1):
        self.logger = logging.getLogger('LAC')
        self.logger.setLevel(loglevel)
        self.logger.propagate = False
        handler = logging.StreamHandler()
        handler.setLevel(loglevel)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def set_websocket_url(self):

        if self.protocol == 'https':
            self.ws_url = f"wss://{self.host}/stream/{self.client_id}"
        else:
            self.ws_url = f"ws://{self.host}:{self.port}/stream/{self.client_id}"

    def request_streaming(self, query_params):
        """!
            Make a POST request to streaming service API to initiate streamming.
            Returns response or raises an appropriate exception if exception occured.
        """
        identity = f"[{threading.currentThread().getName()}-{os.getpid()}][{self.client_id}]"

        try:
            headers = {
                'Accept': 'application/json',
                'X-Auth-Token': self.token
            }

            hostname = f"{self.host}:{self.port}" if self.port != 80 else self.host
            url = f"{self.protocol}://{hostname}/client/{self.client_id}/process/stream"

        except Exception:
            return SERVICE_500_ERROR_RESPONSE, None

        self.logger.debug(f"{identity} Posting to {url} with query params {query_params}")
        try:
            service_response = requests.post(url, headers=headers, params=query_params)
        except RequestException:
            return SERVICE_500_ERROR_RESPONSE, None

        self.logger.info(f"{identity}: Service responded with status {service_response.status_code}")

        try:
            response = service_response.json()
        except Exception:
            response = {'code': service_response.status_code}

        self.logger.debug(f"{identity} Service responded with response {response}")

        return service_response.status_code, response

    def init_task_processing(self, status, response, channels):
        self.is_final = False

        if status is not HTTP_200_OK:
            self.failure = "Service responded with invalid status: {}".format(status)
            return False

        try:
            process_id = response['pid']
            process_task_request = DEFAULT_HANDSHAKE_PACKET
            process_task_request['pid'] = process_id
            process_task_request['channels'] = channels
            self.websocket.send(json.dumps(process_task_request), opcode=websocket.ABNF.OPCODE_TEXT)
        except Exception as e:
            self.failure = "Failure on handshake process, message: {}".format(e)
            return False

        return True

    def end_task_processing(self):
        identity = "[{}-{}][{}]".format(threading.currentThread().getName(), os.getpid(), self.client_id)

        self.logger.info(f"{identity} Notify service that audio file transmission ended")

        try:
            self.websocket.send(json.dumps(CLOSE_HANDSHAKE_PACKET), opcode=websocket.ABNF.OPCODE_TEXT)
            self.processing_state = 'idle'
        except Exception as err:
            self.logger.info(f"{identity} Failure in sending end task notification, error: {err}")
            return False

        return True

    def keepalive(self, process_id):
        identity = "[{}-{}][{}]".format(threading.currentThread().getName(), os.getpid(), self.client_id)

        self.logger.info(f"{identity} Sending application level keep-alive request")

        try:
            keepalive_request = KEEPALIVE_CONTROL_PACKET
            keepalive_request['pid'] = process_id

            self.websocket.send(json.dumps(keepalive_request), opcode=websocket.ABNF.OPCODE_TEXT)

        except Exception as e:
            self.logger.info(f"{identity} Failure in sending keep-alive ping control, error: {e}")
            return False

        return True

    def on_open(self):
        self.logger.info(f"{self.identity} Client connected at {self.ws_url}")

    def on_message(self, *message):
        """Handles new data received from the streaming server"""
        data = message[0]

        self.logger.debug(f"{self.identity} on_message len: {len(data)}")
        self.logger.debug(f"{self.identity} on_message data ({type(data)}): {data}")
        try:
            if isinstance(data, str):
                data = json.loads(data)
        except Exception as err:
            self.logger.error(f"{self.identity} Error while parsing data: {err}")
            pass

        if isinstance(data, dict):
            if data.get('error') or data.get('status') or data.get('is_final'):
                self.logger.debug(f"{self.identity} ws: {self.ws_url}, data: {data}")

            if data.get('error'):
                self.failure = data.get('error')

            elif data.get('status') == 'ready':
                self.processing_state = 'ready'
                self.ws_service_event.set()

            elif data.get('status') == 'loaded':
                try:
                    self.processing_state = 'loaded'
                    self.retry_time = int(data.get('delay'))
                    self.ws_service_event.set()
                except ValueError:
                    self.failure = "Failed to get delay from service response"

            elif data.get('status') == 'accepting':
                self.processing_state = 'accepting'
                self.ws_service_event.set()

            elif data.get('is_final'):
                self.is_final = True
                if self.finish is True:
                    self.logger.info(f"{self.identity} Finish is true, closing websocket")
                    self.ws_service_event.set()
                    self.websocket.close()
                    self.logger.info(f"{self.identity} Websocket closed")

            elif data.get('κeepalive') == 'ack':
                self.logger.info(f"{self.identity} Server sent an application level keepalive ack")

            else:  # service responded with actual results
                error = self.results_handler(data)
                if data.get("alerts"):
                    self.logger.debug("Alerts received")
                    self.ws_service_event.set()

                if error:
                    self.failure = f"Results handler returned error, message: {error}"

            if self.failure:
                self.logger.error(f"{self.identity} Failure: {self.failure}")
                self.websocket.close()

        else:
            self.logger.error(f"{self.identity} Client received invalid data of type {type(data)} : {data}")

            self.processing_state = None
            self.failure = "Failure on server response, message: invalid data type "
            self.logger.error(f"{self.identity} Failure: {self.failure}")
            self.websocket.close()

    def on_error(self, *exception):
        """Handle errors"""
        if self.failure and self.is_final is not True:
            self.logger.error(f"{self.identity} Error on websocket: {exception}")
            self.logger.error(f"{self.identity} Failure: {self.failure}")

        self.processing_state = None

        if not self.failure:
            if self.is_final is not True:
                self.failure = f"Failure on websocket: {exception}"
        if self.websocket:
            self.websocket.close()

    def on_close(self, *args):
        """Function called when server closed the connection"""
        if self.failure and self.is_final is not True:
            self.logger.error(f"{self.identity} Failure: {self.failure}")
        else:
            self.logger.info(f"{self.identity} Closing connection on {self.ws_url}")

        self.processing_state = None

    def run(self):
        self.identity = f"[{threading.currentThread().getName()}-{os.getpid()}][{self.client_id}]"
        try:
            self.websocket.run_forever()
        except Exception as err:
            self.logger.error(f"{self.identity} run_forever crashed with error {err}")
