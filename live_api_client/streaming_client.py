import websocket
import logging
import os
import time
import threading
from multiprocessing import Queue
from tqdm import tqdm

from .live_api_client import LiveApiClient
from .audio_source import AudioSource
from .globals import (
    STREAMING_RATE,
    WS_RESPONSE_TIME,
    MAX_ACCEPTED_RESULTS_DELAY,
    KEEPALIVE_TIME_PERIOD
)
from .utils import get_audio_format_from_name


class StreamingClient(object):

    def setup_connection(self, protocol, host, port, client_id, token, rate=1, loglevel=10,
                         alerts_on=False, dtt=0):
        self.protocol = protocol
        self.host = host
        self.port = port
        self.client_id = client_id
        self.token = token
        self.rate = int(rate)

        self.queue = Queue()
        self.ws_event = threading.Event()

        self.sender_task_id = None
        self.identity = None
        self.loglevel = loglevel
        self.configure_logger(self.loglevel)
        self.failure = None
        self.process_id = None

        self.delay_transmission_time = dtt
        self.alerts_on = alerts_on

        self.live_api_client = LiveApiClient(
            host=host, port=port, client_id=client_id, token=token,
            results_handler=self.results_default_handler, protocol=self.protocol,
            ws_event=self.ws_event,
            loglevel=loglevel
        )

        self.full_res = []

    def configure_logger(self, loglevel=1):
        self.logger = logging.getLogger('BSC')
        self.logger.setLevel(loglevel)
        self.logger.propagate = False
        handler = logging.StreamHandler()
        handler.setLevel(loglevel)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def results_default_handler(self, data):
        self.logger.debug(f"Got {len(str(data))} bytes of response")
        self.full_res.append(data)
        return None

    def run(self, audio_data):
        thread_name = f"SenderThread-{os.getpid()}"
        self.sender_task_id = threading.Thread(name=thread_name, target=self.stream_next_audio,
                                               args=(audio_data,))
        self.sender_task_id.start()
        self.live_api_client.run()

        while self.queue.qsize() > 0:
            self.logger.error(f"{self.identity} {self.queue.get()}")

        return self.full_res, self.process_id

    def delay_with_keepalive(self):
        current_time = time.time()
        delay_end_time = current_time + self.delay_transmission_time

        while True:
            self.live_api_client.keepalive(self.process_id)
            time.sleep(min(KEEPALIVE_TIME_PERIOD, int(delay_end_time - current_time)))
            current_time = time.time()
            if int(current_time - delay_end_time) >= 0:
                break

    def ws_connection_initiation_wait_time(self, wc_connection_wait_time):
        self.ws_event.wait(timeout=wc_connection_wait_time)
        self.logger.info(
            f"{self.identity} Processing state is: {self.live_api_client.processing_state}")
        if not self.ws_event.is_set():
            self.failure = f"Failed to connect to socket after {wc_connection_wait_time} seconds"
            return None
        self.ws_event.clear()
        if self.live_api_client.failure:
            self.failure = self.live_api_client.failure
            return None
        if self.live_api_client.processing_state == 'loaded':
            time.sleep(self.live_api_client.retry_time)
            return None
        elif self.live_api_client.processing_state != 'ready':
            self.failure = f"Service sent invalid state: {self.live_api_client.processing_state}"
            return None
        return None

    def stream_next_audio(self, audio_data):
        self.identity = f"[{threading.currentThread().getName()}][{self.client_id}]"
        self.ws_connection_initiation_wait_time(WS_RESPONSE_TIME)

        if self.failure:
            self.logger.info(
                f"{self.identity} Failed to initiate processing, error: {self.failure}")
            self.live_api_client.websocket.close()

            if self.queue:
                self.queue.put(self.failure)

            return False

        while not self.live_api_client.failure and not self.live_api_client.finish:

            self.logger.info(
                f"{self.identity} Request to stream audio file with {self.rate}x rate and name "
                f"{audio_data['name']}")

            status, response = self.live_api_client.request_streaming(audio_data)

            try:
                self.process_id = response['pid']
            except Exception:
                self.process_id = None

            if self.delay_transmission_time:
                self.delay_with_keepalive()

            retries = 0
            while True:
                create_task = self.live_api_client.init_task_processing(status, response,
                                                                        audio_data.get('channels'))

                if not create_task:
                    if self.queue:
                        self.queue.put(self.failure)

                    return False

                self.ws_event.wait(timeout=WS_RESPONSE_TIME)

                if not self.ws_event.is_set():
                    self.failure = f"Failed receive to connect to socket after {WS_RESPONSE_TIME} " \
                                   f"seconds"

                    if self.queue:
                        self.queue.put(self.failure)

                    return False

                self.ws_event.clear()

                retries += 1

                if not self.live_api_client.failure and self.live_api_client.processing_state == \
                        "loaded":
                    if retries > 5:
                        self.live_api_client.websocket.close()
                        self.failure = f"Service appears to be loaded after {retries} attempts"
                        self.logger.error(f"{self.identity} {self.failure}")

                        if self.queue:
                            self.queue.put(self.failure)

                        return False

                    time.sleep(self.live_api_client.retry_time)
                    continue

                if not self.live_api_client.failure and self.live_api_client.processing_state != \
                        "accepting":
                    self.failure = f"Service responded with unexpected " \
                                   f"status: {self.live_api_client.processing_state}"

                    if self.queue:
                        self.queue.put(self.failure)

                    return False

                break

            if self.live_api_client.failure:
                self.failure = self.live_api_client.failure
                if self.queue:
                    self.queue.put(self.failure)
                return None

            audio_format = get_audio_format_from_name(audio_data['name'])
            try:
                audio_source = AudioSource(audio_data['name'], audio_format,
                                           self.rate * STREAMING_RATE)
            except ValueError as err:
                self.failure = f"Error with audio source: {err}"
                if self.queue:
                    self.queue.put(self.failure)
                self.live_api_client.websocket.close()
                return False

            max_complete_time = round(
                audio_source.getduration() - audio_source.getduration() / self.rate +
                MAX_ACCEPTED_RESULTS_DELAY,
                3)

            self.stream_audio(audio_source)

            # wait for alerts
            if self.alerts_on:
                self.logger.info(f"{self.identity} Waiting for alerts for {max_complete_time} secs")
                self.ws_event.wait(timeout=max_complete_time)
                status = self.check_event(max_complete_time)
                if not status:
                    return status

            self.logger.info(f"{self.identity} Audio finished. Assigning finish")
            self.live_api_client.finish = True

            success = self.live_api_client.end_task_processing()
            self.logger.info(f"{self.identity} Task ended processing with success: {success}")

            if not success and self.queue:
                self.queue.put(self.failure)
                return False

            self.logger.info(f"{self.identity} Waiting for event for {max_complete_time} secs")
            self.ws_event.wait(timeout=max_complete_time)

            status = self.check_event(max_complete_time)
            if not status:
                return status

        if self.live_api_client.failure:
            self.failure = self.live_api_client.failure

        if not self.failure:
            self.logger.info(f"{self.identity} Successfully streamed audio")

        self.logger.info(f"{self.identity} Returning... Failure: {self.failure}")

        return True

    def check_event(self, max_complete_time):
        if not self.ws_event.is_set():
            self.failure = f"Failed to receive service status after {max_complete_time} secs " \
                           f"on end task signal"
            self.logger.error(f"{self.identity} {self.failure}")

            if self.queue:
                self.queue.put(self.failure)

            self.live_api_client.websocket.close()
            return False

        self.logger.info(f"{self.identity} Clearing event")
        self.ws_event.clear()
        return True

    def stream_audio(self, audio_source):
        """
        Stream audio to service by sending audio chunks to websocket
        """
        success = True

        self.logger.info(
            f"{self.identity}[{self.process_id}] Streaming"
            f" audio chunks ({audio_source.chunk_size} bytes) to {self.live_api_client.ws_url}")
        with tqdm(total=audio_source.get_audio_size(),
                  desc=f"{self.identity}[{self.process_id}] Streaming audio chunks ("
                       f"{audio_source.chunk_size} bytes)",
                  disable=(self.loglevel >= 20)) as pbar:
            for data in audio_source:
                try:
                    self.live_api_client.websocket.send(data, websocket.ABNF.OPCODE_BINARY)
                    time.sleep(STREAMING_RATE)
                except Exception as err:
                    self.logger.error(f"{self.identity}[{self.process_id}] Error: {err}")
                    success = False
                    break
                pbar.update(len(data))

        if success and not self.live_api_client.failure:
            self.logger.info(f"{self.identity}[{self.process_id}] Successfully streamed audio")
        else:
            self.logger.error(
                f"{self.identity} Failure: {self.live_api_client.failure}, success: {success}")

        return success
