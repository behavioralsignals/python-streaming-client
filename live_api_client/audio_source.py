import wave
import math
import os
import logging


class AudioSource(object):
    """
    Read audio from a raw file or the DATA section of a WAV, MP3 file in chunks of a specified
    duration.
    """
    ACCEPTED_AUDIO_FORMATS = ['wav']
    ACCEPTED_SAMPLING_RATES = [8000, ]

    def __init__(self, audio_path, audio_fmt, rate, channels=2, loglevel=10):
        """
        audio_path: Path to audio file.
        audio_fmt: Audio format, acceptable audio formats are RAW, WAV, MP3 files of sampling
        rate of 8 kHz.
        chunk_length: Length of chunk in milliseconds.
                      The chunk length should be a multiple of 10ms and in the range from 100ms
                      to 1000ms.
        """
        self.configure_logger(loglevel)

        if audio_fmt not in self.ACCEPTED_AUDIO_FORMATS:
            error = f"Input audio file should be one of the following formats: " \
                    f"{self.ACCEPTED_AUDIO_FORMATS}."
            self.logger.error(error)
            raise ValueError(error)
        self.audio_fmt = audio_fmt

        if self.audio_fmt == 'wav':
            self.input = wave.open(audio_path, 'rb')
            self.framerate = self.input.getframerate()
            self.sampwidth = self.input.getsampwidth()
            self.nchannels = self.input.getnchannels()
            self.nframes = self.input.getnframes()

        if self.getframerate() not in self.ACCEPTED_SAMPLING_RATES:
            error = "Input audio file should have sampling rate of 8 kHz."
            self.logger.error(error)
            raise ValueError(error)

        if self.getsampwidth() != 2:
            raise ValueError("Input audio file should have 16-bit samples.")

        # if chunk_length % 10 != 0 or chunk_length < 100 or chunk_length > 1000:
        #     raise ValueError("Chunk length is too small, too large or not a multiple of 10 ms.")

        self.chunk_size = int(rate * self.getframerate())
        self.eof = False

    def configure_logger(self, loglevel=1):
        self.logger = logging.getLogger('AAC')
        self.logger.setLevel(loglevel)
        self.logger.propagate = False
        handler = logging.StreamHandler()
        handler.setLevel(loglevel)
        formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

    def get_audio_size(self):
        return self.getnframes() * self.getsampwidth() * self.getnchannels()

    def getnchannels(self):
        return self.nchannels

    def getnframes(self):
        return self.nframes

    def getframerate(self):
        return self.framerate

    def getsampwidth(self):
        return self.sampwidth

    def getduration(self):
        return self.getnframes() / self.getframerate()

    def close(self):
        self.input.close()

    def __iter__(self):
        return self

    def __len__(self):
        return math.ceil(self.getnframes() / self.chunk_size)

    def __next__(self):
        if not self.eof:
            if self.audio_fmt == 'wav':
                data = self.input.readframes(self.chunk_size)
            else:
                data = self.input.read(self.chunk_size)

            if len(data) > 0:
                return data

            self.eof = True

        self.close()

        raise StopIteration
