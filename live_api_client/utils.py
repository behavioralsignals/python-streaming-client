import os
import json
import jsonschema
from random import randint


def validate2schema(data, schema):
    """
    data: json to be validated
    schema: json schema definition
    """
    try:
        v = jsonschema.Draft6Validator(json.loads(json.dumps(schema)))

        for error in v.iter_errors(json.loads(json.dumps(data))):
            raise error
    except Exception:
        return False

    return True


def get_audio_format_from_name(audio_name):
    return audio_name.split('.')[-1]


def random_interval(start, end):
    i = randint(start, end - 1)
    j = randint(i + 1, end)

    return range(i, j + 1)


def remove_redundant_entries(redundant_keys, data_list):
    for data in data_list:
        for key in redundant_keys:
            data.pop(key)


def load_json_schema(spec_path, spec_name):
    with open(os.path.join(spec_path, spec_name)) as f:
        schema = json.load(f)

    return schema
