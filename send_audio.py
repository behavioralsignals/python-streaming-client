"""This is just a wrapper to send multiple files to a streaming api. It is
able to submit multiple file at a specified rate simulating a live recording
streaming session. Uses the same dependencies as client.

Copyright: Behavioral Signals Technologies
"""

import os
import argparse
import subprocess


def run(input_path, output_path, client_id, client_token,
        servername="live.behavioralsignals.com", mode="external",
        rate=1, channels=2, extentions=[".wav", ".mp3"]):
    """Main run function

    Args:
        input_path (str): Path to input directory containing the audio files.
        output_path (str): Path to output directory to store the pooled results.
        client_id (str): Client id.
        client token (str): Client token.
        servername (str): The url or IP of the respective API server.
        mode (str): The type of the API server. Can be external or internal.
        rate (int): The ratio of the audio transmission to the duration
            of the audio itself.
        channels (int): The number of channels of the audio files.
        extentions (list): A list of strings corresponding to available
            extentions of the audio files.
    """

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    client = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          "client.py")
    files = os.listdir(input_path)

    for f in files:
        if os.path.splitext(f)[-1] not in extentions:
            continue
        input_file = os.path.join(input_path, f)
        output_file = os.path.join(output_path, os.path.splitext(
                                   os.path.basename(f))[0] + ".json")

        cmd = ("python3 {} -s {} -m {} -i {} -t {} -r {} -c {} {}"
               .format(client, servername, mode, client_id, client_token,
                       rate, channels, input_file))

        break_cmd = cmd.split()
        subprocess.call(break_cmd, stdout=open(output_file, "w"))


def parse_arguments():
    """Parse arguments to send audio to streaming api
    TODO: duplicate from client
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--input_path", required=True,
                        help="Path to audio directory")
    parser.add_argument("-o", "--output_path", required=True,
                        help="Path to result directory")
    parser.add_argument("-s", "--servername",
                        default="live.behavioralsignals.com",
                        help="Streaming API Server Hostname")
    parser.add_argument("-m", "--mode", default="external",
                        help=argparse.SUPPRESS)
    parser.add_argument("-i", "--client_id", required=True,
                        help="Client account ID")
    parser.add_argument("-t", "--client_token", required=True,
                        help="Client Token")
    parser.add_argument("-r", "--rate", default=1.0, type=float,
                        help="How fast to send the audio compared to actual"
                             " audio playtime (default 1x realtime)")
    parser.add_argument("-c", "--channels", default=2, type=int,
                        choices=[1, 2], help="Audio channels (1, 2ch)")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    run(args.input_path, args.output_path, args.client_id, args.client_token,
        servername=args.servername, mode=args.mode,
        rate=args.rate, channels=args.channels)
